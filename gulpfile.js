const nunjucksRender = require('gulp-nunjucks-render');
const gulp = require('gulp');
const runTimestamp = Math.round(Date.now() / 1000);
const consolidate = require('gulp-consolidate');
const async = require('async');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const data = require('gulp-data');
const responsive = require('gulp-responsive');
const iconfont = require('gulp-iconfont');
const useref = require('gulp-useref');
const refHash = require('gulp-ref-hash');
const clean = require('gulp-clean');
const runSequence = require('run-sequence');
const serve = require('gulp-serve');
var browserSync = require('browser-sync').create();


gulp.task('nunjucks', ['clean'], () => {
  // Gets .html and .nunjucks files in pages
  return gulp.src('src/html/index.html')
    // Renders template with nunjucks
    .pipe(data(() => (require('./src/data/app.json'))))
    .pipe(nunjucksRender({
      path: ['src/html/templates/']
    }))
    .pipe(refHash({
      paths: {
        js: '/dist/',
        css: '/dist/',
      }
    })) // Generate hashed filenames for the build blocks
    .pipe(useref()) // R
    .pipe(gulp.dest('./'))
});

gulp.task('clean', () => (
  gulp.src('dist/')
    .pipe(clean())
))

gulp.task('iconfont', (done) => {
  var iconStream = gulp.src(['src/images/*.svg'])
    .pipe(iconfont({ fontName: 'hero', fontHeight: '30px' }));

  async.parallel([
    function handleGlyphs(cb) {
      iconStream.on('glyphs', (glyphs, options) => {
        gulp.src('src/fonts/_hero.css')
          .pipe(consolidate('lodash', {
            glyphs: glyphs,
            fontName: 'hero',
            fontPath: '../src/fonts/',
            className: 'hero'
          }))
          .pipe(gulp.dest('src/css/'))
          .on('finish', cb);
      });
    },
    function handleFonts(cb) {
      iconStream
        .pipe(gulp.dest('src/fonts/'))
        .on('finish', cb);
    }
  ], done);
});

gulp.task('images-heroes', () => {
  return gulp.src('src/images/heroes/*.{png,jpg,jpeg}')
    .pipe(responsive({
      '*.{jpg,jpeg}': [{
        width: 200,
        rename: {
          suffix: '-200px',
          extname: '.jpg',
        },
        format: 'jpeg',
      }],
      '*.png': [{
        width: 200,
        rename: {
          suffix: '-200px',
          extname: '.png',
        },
        format: 'png',
      }]
    },
      {
        quality: 80
      })).pipe(gulp.dest('src/images/dist/'));
});

gulp.task('images-other', () => {
  return gulp.src('src/images/*.{png,jpg}')
    .pipe(responsive({
      'bg-blue.jpg': [{
        width: 30,
        rename: {
          suffix: '',
          extname: '.jpg',
        },
        format: 'jpeg',
      }],
      'hiway.png': [{
        width: 150,
        rename: {
          suffix: '-sm',
          extname: '.png',
        },
        format: 'png',
      },
      ],
      'icon.jpg': [{
        width: 16,
        rename: {
          suffix: '-16',
          extname: '.jpg',
        },
        format: 'jpeg',
      }, {
        width: 32,
        rename: {
          suffix: '-32',
          extname: '.jpg',
        },
        format: 'jpeg',
      }, {
        width: 144,
        rename: {
          suffix: '-144',
          extname: '.jpg',
        },
        format: 'jpeg',
      }, {
        width: 114,
        rename: {
          suffix: '-114',
          extname: '.jpg',
        },
        format: 'jpeg',
      }, {
        width: 72,
        rename: {
          suffix: '-72',
          extname: '.jpg',
        },
        format: 'jpeg',
      }, {
        width: 57,
        rename: {
          suffix: '-57',
          extname: '.jpg',
        },
        format: 'jpeg',
      }],
      'logo.png': [{
        width: 50,
        rename: {
          suffix: '-sm',
          extname: '.png',
        },
        format: 'png',
      },
      {
        width: 820,
        rename: {
          suffix: '-main',
          extname: '.png',
        },
        format: 'png',
      },
      ]
    },
      {
        quality: 80
      })).pipe(gulp.dest('src/images/dist/'));
});

gulp.task('css', () => {
  return gulp.src('src/css/index.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS({ debug: true, compatibility: 'ie8', specialComments: false }, (details) => {
      console.log(`${details.name}: ${details.stats.originalSize}`);
      console.log(`${details.name}: ${details.stats.minifiedSize}`);
    }))
    .pipe(gulp.dest('src/css/'));
});

gulp.task('dev', ['watch', 'serve']);

gulp.task('reload', ['nunjucks'], function (done) {
  browserSync.reload();
  done();
});


gulp.task('serve', serve('./'));

gulp.task('watch', function () {
  browserSync.init({
    server: {
      baseDir: "./"
    }
  });
  return [
    gulp.watch(['src/html/templates/**/*.html', 'src/html/index.html', 'src/js/**/*.js', 'src/data/**/*.json'], ['reload']),
    gulp.watch(['src/css/**/*.scss'], () => { runSequence('css', 'reload') }),
  ];
})


gulp.task('build', ['clean', 'nunjucks', 'css'])